package dirty

import (
	"bufio"
)

type Array []Element

func (Array) isElement() {}
func (Array) getType() ElementType {
	return ElemArray
}

type Element interface {
	isElement()
	getType() ElementType
}

type ElementType int

const (
	ElemArray ElementType = iota
	ElemString
	ElemConst
	ElemInt
	ElemFloat
)

func loadArray(r *bufio.Reader) ([]Element, error) {
	topArray := []Element{}
	for {
		t, err := nextToken(r)
		if err != nil {
			return []Element{}, err
		}
		//debugf("in LoadArray got %+v\n", t)

		switch t.ttype {
		case LBRACKET:
			//debugf("in LoadArray loading array\n")
			array, err := loadArray(r)
			if err != nil {
				return []Element{}, err
			}
			topArray = append(topArray, Array(array))
		case RBRACKET:
			//debugf("in LoadArray closing\n")
			return topArray, nil
			// todo atoms
		case COMMENT:
			continue
		case STRING:
			//debugf("in LoadArray adding string\n")
			topArray = append(topArray, String(t.t))
		case STRING_RAW:
			//debugf("in LoadArray adding raw string\n")
			topArray = append(topArray, String(t.t))
		case CONST:
			//debugf("in LoadArray adding const\n")
			topArray = append(topArray, NewConst(t.t))
		case NUMBER:
			//debugf("in LoadArray adding number %+v, %d\n", t, *t.i)
			if t.i != nil {
				topArray = append(topArray, Int(*t.i))
			}
		case FLOAT:
			//debugf("in LoadArray adding float %+v, %f\n", t, *t.f)
			if t.f != nil {
				topArray = append(topArray, Float(*t.f))
			}
		default:
			//debugln("loadArray")
			return []Element{}, NewSyntaxError(t, []token{})
		}
	}
}
