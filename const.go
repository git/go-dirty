package dirty

import (
	"fmt"
)

type Const int

const (
	TRUE Const = iota
	FALSE
	NULL
)

func NewConst(s string) Const {
	if s == "true" {
		return TRUE
	}
	if s == "false" {
		return FALSE
	}
	if s == "null" {
		return NULL
	}
	panic("invalid const " + s)
}

func (Const) isElement() {}
func (Const) getType() ElementType {
	return ElemConst
}
func (c Const) String() string {
	if c == TRUE {
		return "true"
	}
	if c == FALSE {
		return "false"
	}
	if c == NULL {
		return "null"
	}
	panic(fmt.Sprintf("invalid const %d", c))
}
func (c Const) Bool() bool {
	if c == TRUE {
		return true
	} else if c == FALSE {
		return false
	} else {
		panic("Const is not bool")
	}
}

func parseConst(t token) (token, error) {
	if t.t != "true" && t.t != "false" && t.t != "null" {
		return token{}, NewConstError(t.t)
	}
	return t, nil
}
