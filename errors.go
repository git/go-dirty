package dirty

import (
	"fmt"
)

type SyntaxError struct {
	found    token
	expected []token
}

func NewSyntaxError(found token, expected []token) SyntaxError {
	return SyntaxError{found: found, expected: expected}
}
func (se SyntaxError) Error() string {
	return fmt.Sprintf("expected %v; got %v\n", se.expected, se.found)
}

type UnterminatedError struct {
	ttype string
	t     string
}

func NewUnterminatedError(ttype string, t string) UnterminatedError {
	return UnterminatedError{ttype: ttype, t: t}
}
func (e UnterminatedError) Error() string {
	return fmt.Sprintf("unterminated %s ‘%s’\n", e.ttype, e.t)
}

type InvalidCharError struct {
	r rune
}

func NewInvalidCharError(r rune) InvalidCharError {
	return InvalidCharError{r: r}
}
func (e InvalidCharError) Error() string {
	return fmt.Sprintf("invalid character ‘%d’\n", e.r)
}

type CommaError struct {
	s string
}

func NewCommaError(s string) CommaError {
	return CommaError{s: s}
}
func (e CommaError) Error() string {
	return fmt.Sprintf("comma in wrong place in ‘%s’\n", e.s)
}

type InvalidCodepointError struct {
	c string
}

func NewInvalidCodepointError(c string) InvalidCodepointError {
	return InvalidCodepointError{c: c}
}
func (e InvalidCodepointError) Error() string {
	return fmt.Sprintf("invalid codepoint ‘%s’\n", e.c)
}

type ConstError struct {
	t string
}

func NewConstError(t string) ConstError {
	return ConstError{t: t}
}
func (e ConstError) Error() string {
	return fmt.Sprintf("malformed const ‘%s’\n", e.t)
}

type EscapeError struct {
	char rune
}

func NewEscapeError(char rune) EscapeError {
	return EscapeError{char: char}
}
func (e EscapeError) Error() string {
	return fmt.Sprintf("invalid escape sequence \\%v\n", e.char)
}

type RawStringError struct {
	s string
}

func NewRawStringError(s string) RawStringError {
	return RawStringError{s: s}
}
func (e RawStringError) Error() string {
	return e.s
}
